import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 5,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color(0xFF319795),
                Color(0xFF3182CE),
              ],
            ),
          ),
        ),
        Container(
          height: 60,
          alignment: Alignment.centerRight,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(12),
              bottomRight: Radius.circular(
                12,
              ),
            ),
            boxShadow: [
              BoxShadow(
                color: Color(0xFF000029),
                offset: Offset(0, 0.5),
                blurRadius: 0.5,
                spreadRadius: 0.5,
              ),
            ],
          ),
          child: const Padding(
            padding: EdgeInsets.only(right: 20),
            child: Text(
              "Login",
              style: TextStyle(
                fontFamily: "Lato",
                color: Color(
                  0xFF319795,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
