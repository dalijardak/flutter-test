import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Description extends StatelessWidget {
  final Map<String, String> details;

  const Description(this.details);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          fit: FlexFit.loose,
          child: Text(
            details["title"]!,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontFamily: 'Lato',
              fontSize: 21,
              color: Color(0xFF4A5568),
            ),
          ),
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Padding(
            padding: const EdgeInsets.only(left: 100),
            child: SvgPicture.asset(
              "assets/${details['image_path1']}",
              height: 145,
              width: 220,
            ),
          ),
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Row(
            children: [
              const Flexible(
                fit: FlexFit.loose,
                child: Text(
                  "1.",
                  style: TextStyle(
                    fontSize: 130,
                    color: Color(
                      0xFF718096,
                    ),
                  ),
                ),
              ),
              Flexible(
                fit: FlexFit.loose,
                child: Text(
                  details["1"]!,
                  style: const TextStyle(
                    fontSize: 15.75,
                    letterSpacing: 0.47,
                    color: Color(
                      0xFF718096,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),

        //2nd Element
        Padding(
          padding: const EdgeInsets.only(left: 50),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Flexible(
                fit: FlexFit.loose,
                child: Text(
                  "2.",
                  style: TextStyle(
                    fontSize: 130,
                    color: Color(
                      0xFF718096,
                    ),
                  ),
                ),
              ),
              Flexible(
                child: Text(
                  details["2"]!,
                  style: const TextStyle(
                    fontSize: 15.75,
                    letterSpacing: 0.47,
                    color: Color(
                      0xFF718096,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Padding(
            padding: const EdgeInsets.only(left: 100),
            child: SvgPicture.asset(
              "assets/${details['image_path2']}",
              height: 127,
              width: 182,
            ),
          ),
        ),
        //3rd Element
        Flexible(
          fit: FlexFit.loose,
          child: Padding(
            padding: const EdgeInsets.only(left: 70),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Flexible(
                  fit: FlexFit.loose,
                  child: Text(
                    "3.",
                    style: TextStyle(
                      fontSize: 130,
                      color: Color(
                        0xFF718096,
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: Text(
                    details["3"]!,
                    style: const TextStyle(
                      fontSize: 15.75,
                      letterSpacing: 0.47,
                      color: Color(
                        0xFF718096,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Padding(
            padding: const EdgeInsets.only(left: 100),
            child: SvgPicture.asset(
              "assets/${details['image_path3']}",
              height: 127,
              width: 182,
            ),
          ),
        ),
      ],
    );
  }
}
