import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:test/utils.dart';
import 'package:test/widgets/fade_in_indexed_stack.dart';
import 'package:test/widgets/mobile/description.dart';

class Scrolling extends StatefulWidget {
  const Scrolling({Key? key}) : super(key: key);

  @override
  State<Scrolling> createState() => _ScrollingState();
}

class _ScrollingState extends State<Scrolling> {
  int selectedIndex = 0;
  Widget _customButton(
      {required String text, required bool isSelected, required int number}) {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          selectedIndex = number;
        });
      },
      style: ElevatedButton.styleFrom(
        primary:
            selectedIndex == number ? const Color(0xFF81E6D9) : Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: number == 0
              ? const BorderRadius.only(
                  topLeft: Radius.circular(12),
                  bottomLeft: Radius.circular(12),
                )
              : number == 2
                  ? const BorderRadius.only(
                      topRight: Radius.circular(12),
                      bottomRight: Radius.circular(12),
                    )
                  : BorderRadius.zero,
          side: BorderSide(
            color: selectedIndex == number
                ? const Color(0xFFCBD5E0)
                : const Color(0xFF81E6D9),
          ),
        ),
      ),
      child: Text(
        text,
        style: TextStyle(
          color: isSelected ? Colors.white : const Color(0xFF81E6D9),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            const Text(
              "Deine Job\nWebsite",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Lato',
                fontSize: 42,
                letterSpacing: 1.26,
                color: Color(0xFF2D3748),
              ),
            ),
            SvgPicture.asset(
              "assets/undraw_agreement_aajr.svg",
              height: 405,
              width: 418,
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _customButton(
              text: "Arbeitnehmer",
              isSelected: selectedIndex == 0,
              number: 0,
            ),
            _customButton(
              text: "Arbeitgeber",
              isSelected: selectedIndex == 1,
              number: 1,
            ),
            _customButton(
              text: "Temporärbüro",
              isSelected: selectedIndex == 2,
              number: 2,
            )
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        FadeIndexedStack(
          index: selectedIndex,
          duration: const Duration(milliseconds: 500),
          children: const [
            Description(details1),
            Description(details2),
            Description(details3),
          ],
        ),
      ],
    );
  }
}
