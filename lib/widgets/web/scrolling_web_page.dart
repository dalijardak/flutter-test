import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test/utils.dart';
import 'package:test/widgets/bottom.dart';
import 'package:test/widgets/fade_in_indexed_stack.dart';
import 'package:test/widgets/mobile/description.dart';
import 'package:test/widgets/web/web_description.dart';

class ScrollingWebPage extends StatefulWidget {
  const ScrollingWebPage({Key? key}) : super(key: key);

  @override
  State<ScrollingWebPage> createState() => _ScrollingWebPageState();
}

class _ScrollingWebPageState extends State<ScrollingWebPage> {
  int selectedIndex = 0;
  bool isVisible = false;
  ScrollController scrollController = ScrollController();
  Widget _customButton(
      {required String text, required bool isSelected, required int number}) {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          selectedIndex = number;
        });
      },
      style: ElevatedButton.styleFrom(
        primary:
            selectedIndex == number ? const Color(0xFF81E6D9) : Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: number == 0
              ? const BorderRadius.only(
                  topLeft: Radius.circular(12),
                  bottomLeft: Radius.circular(12),
                )
              : number == 2
                  ? const BorderRadius.only(
                      topRight: Radius.circular(12),
                      bottomRight: Radius.circular(12),
                    )
                  : BorderRadius.zero,
          side: BorderSide(
            color: selectedIndex == number
                ? const Color(0xFFCBD5E0)
                : const Color(0xFF81E6D9),
          ),
        ),
      ),
      child: Text(
        text,
        style: TextStyle(
          color: isSelected ? Colors.white : const Color(0xFF81E6D9),
        ),
      ),
    );
  }

  @override
  void initState() {
    scrollController.addListener(() {
      if (scrollController.position.pixels > 300) {
        if (!isVisible) {
          setState(() {
            isVisible = true;
          });
        }
      }
      if (scrollController.position.pixels < 300) {
        if (isVisible) {
          setState(() {
            isVisible = false;
          });
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView(
          controller: scrollController,
          padding: const EdgeInsets.only(top: 20),
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    const Text(
                      "Deine Job\nWebsite",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 60,
                        letterSpacing: 1.7,
                        color: Color(0xFF2D3748),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      height: 40,
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [
                            Color(0xFF319795),
                            Color(0xFF3182CE),
                          ],
                        ),
                      ),
                      child: TextButton(
                        onPressed: () {},
                        child: const Text(
                          "Kostenlos Registrieren",
                          style: TextStyle(
                              fontFamily: "Lato",
                              letterSpacing: 0.84,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
                Flexible(
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 220.0,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(220.0),
                      child: SvgPicture.asset(
                        "assets/undraw_agreement_aajr.svg",
                        height: 405,
                        width: 418,
                      ),
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 140,
                  height: 40,
                  child: _customButton(
                    text: "Arbeitnehmer",
                    isSelected: selectedIndex == 0,
                    number: 0,
                  ),
                ),
                SizedBox(
                  width: 140,
                  height: 40,
                  child: _customButton(
                    text: "Arbeitgeber",
                    isSelected: selectedIndex == 1,
                    number: 1,
                  ),
                ),
                SizedBox(
                  width: 140,
                  height: 40,
                  child: _customButton(
                    text: "Temporärbüro",
                    isSelected: selectedIndex == 2,
                    number: 2,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            FadeIndexedStack(
              index: selectedIndex,
              duration: const Duration(milliseconds: 500),
              children: const [
                WebDescription(details1),
                WebDescription(details2),
                WebDescription(details3),
              ],
            ),
          ],
        ),
        AnimatedSwitcher(
          duration: const Duration(
            milliseconds: 1000,
          ),
          child: isVisible
              ? Container(
                  key: UniqueKey(),
                  height: 70,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xFF000029),
                        blurRadius: 1,
                        spreadRadius: 1,
                      ),
                    ],
                  ),
                  child: Align(
                    child: Container(
                      height: 40,
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [
                            Color(0xFF319795),
                            Color(0xFF3182CE),
                          ],
                        ),
                      ),
                      child: TextButton(
                        onPressed: () {},
                        child: const Text(
                          "Kostenlos Registrieren",
                          style: TextStyle(
                              fontFamily: "Lato",
                              letterSpacing: 0.84,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                )
              : SizedBox(
                  key: UniqueKey(),
                ),
        ),
      ],
    );
  }
}
