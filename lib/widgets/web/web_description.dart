import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WebDescription extends StatelessWidget {
  final Map<String, String> details;

  const WebDescription(this.details);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          fit: FlexFit.loose,
          child: Text(
            details["title"]!,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontFamily: 'Lato',
              fontSize: 34,
              color: Color(0xFF4A5568),
            ),
          ),
        ),

        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              fit: FlexFit.loose,
              child: Padding(
                padding: const EdgeInsets.only(left: 150),
                child: SvgPicture.asset(
                  "assets/${details['image_path1']}",
                  height: 227,
                  width: 341,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 150),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Flexible(
                    fit: FlexFit.loose,
                    child: Text(
                      "1.",
                      style: TextStyle(
                        fontSize: 130,
                        color: Color(
                          0xFF718096,
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.loose,
                    child: Text(
                      details["1"]!,
                      style: const TextStyle(
                        fontSize: 15.75,
                        letterSpacing: 0.47,
                        color: Color(
                          0xFF718096,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),

        //2nd Element
        Padding(
          padding: const EdgeInsets.only(top: 100),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                fit: FlexFit.loose,
                flex: 1,
                child: SvgPicture.asset(
                  "assets/${details['image_path2']}",
                  height: 227,
                  width: 324,
                ),
              ),
              const SizedBox(
                width: 40,
              ),
              const Flexible(
                fit: FlexFit.loose,
                child: Text(
                  "2.",
                  style: TextStyle(
                    fontSize: 130,
                    color: Color(
                      0xFF718096,
                    ),
                  ),
                ),
              ),
              Flexible(
                fit: FlexFit.loose,
                child: Text(
                  details["2"]!,
                  style: const TextStyle(
                    fontSize: 15.75,
                    letterSpacing: 0.47,
                    color: Color(
                      0xFF718096,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),

        //3rd Element
        Padding(
          padding: const EdgeInsets.only(top: 100),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "3.",
                    style: TextStyle(
                      fontSize: 130,
                      color: Color(
                        0xFF718096,
                      ),
                    ),
                  ),
                  Text(
                    details["3"]!,
                    style: const TextStyle(
                      fontSize: 15.75,
                      letterSpacing: 0.47,
                      color: Color(
                        0xFF718096,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 100),
          child: SvgPicture.asset(
            "assets/${details['image_path3']}",
            height: 376,
            width: 502,
          ),
        ),
      ],
    );
  }
}
