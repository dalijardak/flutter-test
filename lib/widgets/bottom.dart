import 'package:flutter/material.dart';

class Bottom extends StatelessWidget {
  const Bottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.center,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(
            12,
          ),
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color(0xFF000029),
            blurRadius: 1,
            spreadRadius: 1,
          ),
        ],
      ),
      child: Align(
        child: Container(
          height: 40,
          padding: const EdgeInsets.symmetric(horizontal: 30),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color(0xFF319795),
                Color(0xFF3182CE),
              ],
            ),
          ),
          child: TextButton(
            onPressed: () {},
            child: const Text(
              "Kostenlos Registrieren",
              style: TextStyle(
                  fontFamily: "Lato", letterSpacing: 0.84, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
