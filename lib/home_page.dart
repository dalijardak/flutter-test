import 'package:flutter/material.dart';
import 'package:test/widgets/bottom.dart';
import 'package:test/widgets/login.dart';
import 'package:test/widgets/mobile/scrolling_widget.dart';
import 'package:test/widgets/web/scrolling_web_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    bool isMobile = MediaQuery.of(context).size.width < 850;
    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              const Login(),
              Expanded(
                  child:
                      isMobile ? const Scrolling() : const ScrollingWebPage()),
              isMobile ? const Bottom() : const SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
