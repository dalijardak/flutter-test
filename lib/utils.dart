const Map<String, String> details1 = {
  "title": "Drei einfache Schritte\nzu deinem neuen Job",
  "image_path1": "undraw_Profile_data_re_v81r.svg",
  "image_path2": "undraw_task_31wc.svg",
  "image_path3": "undraw_personal_file_222m.svg",
  "1": "Erstellen dein Lebenslauf",
  "2": "Erstellen dein Lebenslauf",
  "3": "Mit nur einem Klick bewerben",
};
const Map<String, String> details2 = {
  "title": "Drei einfache Schritte\nzu deinem neuen Mitarbeiter",
  "image_path1": "undraw_Profile_data_re_v81r.svg",
  "image_path2": "undraw_about_me_wa29.svg",
  "image_path3": "undraw_swipe_profiles1_i6mr.svg",
  "1": "Erstellen dein Unternecheimsprofil",
  "2": "Erstellen ein Jobinserat",
  "3": "Wähle deinen neuen Mitarbeiter aus",
};
const Map<String, String> details3 = {
  "title": "Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter",
  "image_path1": "undraw_Profile_data_re_v81r.svg",
  "image_path2": "undraw_job_offers_kw5d.svg",
  "image_path3": "undraw_business_deal_cpi9.svg",
  "1": "Erstellen dein Lebenslauf",
  "2": "Erhalte Vermittlungs- angebot von Arbeitgeber",
  "3": "Vermittlung nach Provision oder Stundenlohn",
};
